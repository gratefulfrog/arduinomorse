
#include "pitches.h"
#include "defs.h"
#include "node.h"

const int buzzerPin = 8,
          pbPin     = 9,
          ledPin    = LED_BUILTIN,
          toneTime = 500,
          toneEpsilon = 50,
          dahTime = toneTime-toneEpsilon,
          ditTime = dahTime/4,
          morseNoteIndex = 0,
          maxDitTime = 250;
                
// notes to play:
int notes[] = {NOTE_C4,
               NOTE_D4,
               NOTE_E4,
               NOTE_F4,
               NOTE_G4,
               NOTE_A4, 
               NOTE_B4,
               NOTE_C5},
    nbNotes = 8;

void doTone(const int i,const int t,boolean ser = true){
  unsigned long now = millis();
  digitalWrite(ledPin,HIGH);
  tone(buzzerPin, notes[i],t);
  delay(t);
  unsigned long duration = millis()-now;
  if (ser) {
    Serial.print(duration > maxDitTime ? "dah " : "dit ");
  }
  digitalWrite(ledPin,LOW);
  delay(toneEpsilon);  
  //delay(toneTime-t);  
}
void dah(){
  doTone(morseNoteIndex,dahTime);
}
void dit(){
  doTone(morseNoteIndex,ditTime);
}
void eol(){
  delay(toneTime);
  Serial.println();
}

const wordOut bWord = {"bonjour",{b,o,n,j,o,u,r}},
              cWord = {"ciao",{c,i,a,o}}; 

const String ciao = "ciao",
             bonjour = "bonjour";
 
void sendWordOut(const String &outWord){
  Serial.println(outWord);
  for (int i=0;i<outWord.length();i++){
    doChar(outWord.charAt(i));
  }
  //eol();
}

void setup() {
  Serial.begin(115200);
  pinMode(buzzerPin,OUTPUT); 
  pinMode(pbPin,INPUT_PULLUP);
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin,LOW);
  //sendWordOut(bonjour);
  //sendWordOut(ciao);
}

toneFuncPtr char2Func(char ci){
  static const toneFuncPtr fVec[] = {a,b,c,           // a b c
                                    NULL,NULL,NULL,  // d e f 
                                    NULL, NULL, i,   // g h i
                                    j, NULL,NULL,    // j k l
                                    NULL, n,o,       // m n o
                                    NULL, NULL, r,   // p q r
                                    s, NULL, u,      // s t u 
                                    NULL,NULL,NULL,  // v w x
                                    NULL,NULL};      // y z
                                
  String sVal = String(ci);
  sVal.trim();
  sVal.toLowerCase();
  char cVal = sVal.charAt(0);
  int index = int(cVal)-97;
  return fVec[index];
} 

void doChar(char ca){
  toneFuncPtr fPtr = char2Func(ca);
  if (fPtr){
    Serial.print(ca);
    Serial.print(" : ");
    (*fPtr)();
    eol();
  }
}

int getInc(const int i){
  static int res = 1;
  if (i == nbNotes -1){
    res = -1;
  }
  else if (i == 0){
    res = 1;
  }
  return res;
}

boolean readFromSerial(){
  // return true if word is complete and output sent
  static unsigned long lastRead = 0;
  static boolean bRead = false;
  static String word = "";
  if (Serial.available()>0){
    word += String(char(Serial.read()));
    lastRead = millis();
    bRead = true;
  }
  if ((bRead) && (millis() - lastRead > 200)){
    //Serial.println(String("sending word: ") + word);
    sendWordOut(word);
    word = "";
    bRead = false;
  }
  return bRead;
}

void loop(){
  static boolean readFullWord =  false;
  if (readFromSerial()){
    readFullWord = true;
  }
  else {
    if (readFullWord){  // we previously read fll word, but not this time so we do new line 
      Serial.println();
      readFullWord = false;  
    }
  }
  if (!digitalRead(pbPin)){
    delay(30);
    unsigned long now = millis();
    while(! digitalRead(pbPin));
    if (millis()-now > maxDitTime){
      dah();
    }
    else{
      dit();
    }
    readFullWord = true;
  }
}
