#ifndef MORSER_H
#define MORSER_H

#include <Arduino.h>
#include "pitches.h"
#include "node.h"

class Morser;
typedef void (*toneFuncPtr)(const Morser&);

typedef void (*ddFuncPtr)(const Morser&);

class Morser {
   private:
    void doTone(const int toneTime,boolean ser = true)  const;
    static void dah(const Morser&);
    static void dit(const Morser&); 
    void eol() const;
    void sendWordOut(const String &outWord) const;
    static int char2ArrayIndex(char ci);
    void doChar(char ca) const;
    boolean readFromSerial() const;
    void interpretPbPress() const;

    const int buzzerPin,     // buzzer should be connected via resistor to GND
              pbPin,         // pb should be pulled down to GND when pushed
              ledPin,        // LED should be connected via resistor to GND
              toneFreq;      // tone frequency to buzz
    
    const static int  toneTime       = 500,
                      toneEpsilon    = 50,
                      dahTime        = toneTime-toneEpsilon,
                      ditTime        = dahTime/4,
                      maxDitTime     = 250,
                      maxDDWait      = 2*dahTime,
                      serialReadWait = 200,
                      deounceDelay   = 30,
                      maxDd          = 4;
     const static ddFuncPtr ddARray[][5];
    
  public:
    Morser(int buzzerPin, int pbPin, int ledPin = LED_BUILTIN, int toneFreq = NOTE_C4);
    void loop() const;

};

#endif
