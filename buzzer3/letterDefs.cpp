#include "morser.h"

const ddFuncPtr Morser::ddARray[][5] = {  { // a
                                            Morser::dit,
                                            Morser::dah,
                                            NULL},
                                          { // b
                                            Morser::dah,
                                            Morser::dit,
                                            Morser::dit,
                                            Morser::dit,
                                          NULL},
                                          { // c
                                            Morser::dah,
                                            Morser::dit,
                                            Morser::dah,
                                            Morser::dit,
                                          NULL},
                                          { // d
                                            Morser::dah,
                                            Morser::dit,
                                            Morser::dit,
                                          NULL},
                                          { // e
                                            Morser::dit,
                                          NULL},
                                          { // f
                                            Morser::dit,
                                            Morser::dit,
                                            Morser::dah,
                                            Morser::dit,
                                          NULL},
                                          { // g
                                            Morser::dah,
                                            Morser::dah,
                                            Morser::dit,
                                          NULL},
                                          { // h
                                            Morser::dit,
                                            Morser::dit,
                                            Morser::dit,
                                            Morser::dit,
                                          NULL},
                                          { // i
                                            Morser::dit,
                                            Morser::dit,
                                          NULL},
                                          { // j
                                            Morser::dit, 
                                            Morser::dah, 
                                            Morser::dah, 
                                            Morser::dah, 
                                          NULL},
                                          { // k
                                            Morser::dah,
                                            Morser::dit,
                                            Morser::dah,
                                          NULL},
                                          { // l
                                            Morser::dit,
                                            Morser::dah,
                                            Morser::dit,
                                            Morser::dit,
                                          NULL},
                                          { // m
                                            Morser::dah,
                                            Morser::dah,
                                          NULL},
                                          { // n
                                            Morser::dah, 
                                            Morser::dit, 
                                          NULL},
                                          { // o
                                            Morser::dah, 
                                            Morser::dah, 
                                            Morser::dah,  
                                          NULL},
                                          { // p
                                            Morser::dit,
                                            Morser::dah,
                                            Morser::dah,
                                            Morser::dit,
                                          NULL},
                                          { // q
                                            Morser::dah,
                                            Morser::dah,
                                            Morser::dit,
                                            Morser::dah,
                                          NULL},
                                          { // r
                                            Morser::dit, 
                                            Morser::dah, 
                                            Morser::dit, 
                                          NULL},
                                          { // s
                                            Morser::dit,
                                            Morser::dit,
                                            Morser::dit,
                                          NULL},
                                          { // t
                                            Morser::dah,
                                          NULL},
                                          { // u
                                            Morser::dit, 
                                            Morser::dit, 
                                            Morser::dah, 
                                          NULL},
                                          { // v
                                            Morser::dit,
                                            Morser::dit,
                                            Morser::dit,
                                            Morser::dah,
                                          NULL},
                                          { // w
                                            Morser::dit,
                                            Morser::dah,
                                            Morser::dah,
                                          NULL},
                                          { // x
                                            Morser::dah,
                                            Morser::dit,
                                            Morser::dit,
                                            Morser::dah,
                                          NULL},
                                          { // y
                                            Morser::dah,
                                            Morser::dit,
                                            Morser::dah,
                                            Morser::dah,
                                          NULL},
                                          { // z
                                            Morser::dah,
                                            Morser::dah,
                                            Morser::dit,
                                            Morser::dit,
                                          NULL}};
