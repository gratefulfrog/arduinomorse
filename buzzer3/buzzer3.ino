  
/// will send outgoing and interpret incoming morse code!

#include "morser.h"

const int buzzerPin = 8,  // buzzer should be connected via resistor to GND
          pbPin     = 9,  // pb should be pulled down to GND when pushed
          ledPin    = LED_BUILTIN;  // LED should be connected via resistor to GND
     

Morser *mor;

void setup() {
  Serial.begin(115200);
  mor = new Morser(buzzerPin,pbPin, ledPin);
  Serial.println("\nEnter letters to send, or tap morse on the button!");
  Serial.println("Ready!");
}

void loop(){
 mor->loop();
}
