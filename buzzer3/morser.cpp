#include "morser.h"

/// will send outgoing and interpret incoming morse code!

void Morser::doTone(const int toneTime,boolean ser)  const{
  unsigned long now = millis();
  digitalWrite(ledPin,HIGH);
  tone(buzzerPin, toneFreq, toneTime);
  delay(toneTime);
  if (ser) {
    Serial.print(millis()-now > maxDitTime ? "dah " : "dit ");
  }
  digitalWrite(ledPin,LOW);
  delay(toneEpsilon);    
}

void Morser::dah(const Morser& mm){
  mm.doTone(dahTime);
}
void Morser::dit(const Morser& mm){
  mm.doTone(ditTime);
}
void Morser::eol() const{
  delay(toneTime);
  Serial.println();
}
 
void Morser::sendWordOut(const String &outWord) const{
  outWord.toLowerCase();
  Serial.println(outWord);
  for (int i=0;i<outWord.length();i++){
    doChar(outWord.charAt(i));
  }
  eol();
}

int Morser::char2ArrayIndex(char ci){                         
  String sVal = String(ci);
  sVal.trim();
  sVal.toLowerCase();
  char cVal = sVal.charAt(0);
  if (cVal < 'a' || cVal > 'z'){
    return -1;
  }
  int index = int(cVal)-97;  // convert from ascii with 'a' == 0
  return index;
} 

void Morser::doChar(char ca) const{
  int toneVecIndex = char2ArrayIndex(ca);
  if (toneVecIndex>=0){
    Serial.print((char)tolower(ca));
    Serial.print(" : ");
    short i = 0;
    while(ddARray[toneVecIndex][i]){
      (*ddARray[toneVecIndex][i++])(*this);
    }
    eol();
  }
}

boolean Morser::readFromSerial() const{
  // return false if word is complete and output sent
  static unsigned long lastRead = 0;
  static boolean bRead = false;
  static String word = "";
  if (Serial.available()>0){
    word += String(char(Serial.read()));
    lastRead = millis();
    bRead = true;
  }
  if ((bRead) && (millis() - lastRead > serialReadWait)){
    sendWordOut(word);
    word = "";
    bRead = false;
  }
  return bRead;
}

void Morser::interpretPbPress() const{
  static node *currentNodePtr = &start_node;
  static int ddCount = 0;
  static long lastOutputTime = 0;
  
   if (!digitalRead(pbPin)){ // got a press
    delay(deounceDelay);
    unsigned long now = millis();
    while(! digitalRead(pbPin));
    if (millis()-now > maxDitTime){
      dah(*this);
      currentNodePtr = currentNodePtr ? currentNodePtr->dahPtr : NULL;
    }
    else{
      dit(*this);
      currentNodePtr = currentNodePtr ? currentNodePtr->ditPtr : NULL;
    }
    ddCount++;
    lastOutputTime = millis();
  }
  if (millis()-lastOutputTime > maxDDWait){
    if (currentNodePtr == &start_node){
      NULL;
    }
    else if(currentNodePtr != NULL){
      for (int i=0;i<max(0,maxDd -ddCount);i++){
        Serial.print("    ");
      }
      Serial.print(" : "),
      Serial.println(currentNodePtr->val);
      ddCount=0;
    }
    else{
      Serial.println(" : ERROR !!");
      ddCount=0;
    }
    currentNodePtr = &start_node;
  }
}

Morser::Morser(int buzPin, int pbbPin, int leddPin, int tonFreq):
  buzzerPin(buzPin),     // buzzer should be connected via resistor to GND
  pbPin(pbbPin),         // pb should be pulled down to GND when pushed
  ledPin(leddPin),        // LED should be connected via resistor to GND
  toneFreq(tonFreq) {
  pinMode(buzzerPin,OUTPUT); 
  pinMode(pbPin,INPUT_PULLUP);
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin,LOW);
}

void Morser::loop() const{
  if (!readFromSerial()){
    interpretPbPress();
  }
}
