#ifndef NODE_H
#define NODE_H

#include <Arduino.h>

struct node{
  char val;
  const node *const ditPtr,
             *const dahPtr; 
};

const node
// level 0 nodes (leaves)
  q_node     = {'q',NULL,NULL},  
  z_node     = {'z',NULL,NULL},  
  y_node     = {'y',NULL,NULL},
  c_node     = {'c',NULL,NULL},  
  x_node     = {'x',NULL,NULL},  
  b_node     = {'b',NULL,NULL},
  j_node     = {'j',NULL,NULL},  
  p_node     = {'p',NULL,NULL},  
  l_node     = {'l',NULL,NULL},  
  f_node     = {'f',NULL,NULL},
  v_node     = {'v',NULL,NULL},  
  h_node     = {'h',NULL,NULL},  
// level 1 nodes
  o_node     = {'o',NULL,NULL},       // level 1 leaf node, special case
  g_node     = {'g',&z_node,&q_node},  
  k_node     = {'k',&c_node,&y_node},  
  d_node     = {'d',&b_node,&x_node},
  w_node     = {'w',&p_node,&j_node},  
  r_node     = {'r',&l_node,NULL},  
  u_node     = {'u',&f_node,NULL},
  s_node     = {'s',&h_node,&v_node},  
// level 2 nodes
  m_node     = {'m',&g_node,&o_node},
  n_node     = {'n',&d_node,&k_node},
  a_node     = {'a',&r_node,&w_node},
  i_node     = {'i',&s_node,&u_node},
// level 3 nodes
  e_node     = {'e',&i_node,&a_node},
  t_node     = {'t',&n_node,&m_node},
// level 4 node (start node)
  start_node = {'-',&e_node,&t_node};

#endif
