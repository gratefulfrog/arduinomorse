#ifndef MORSER_H
#define MORSER_H

#include <Arduino.h>
#include "pitches.h"
#include "node.h"

class Morser;
typedef void (*toneFuncPtr)(const Morser &m);

class Morser {
   private:
    void doTone(const int toneTime,boolean ser = true)  const;
    void dah() const;
    void dit() const; 
    void eol() const;
    void sendWordOut(const String &outWord) const;
    toneFuncPtr char2Func(char ci) const;
    void doChar(char ca) const;
    boolean readFromSerial() const;

    const int buzzerPin,     // buzzer should be connected via resistor to GND
              pbPin,         // pb should be pulled down to GND when pushed
              ledPin,        // LED should be connected via resistor to GND
              toneFreq,      // tone frequency to buzz
              toneTime       = 500,
              toneEpsilon    = 50,
              dahTime        = toneTime-toneEpsilon,
              ditTime        = dahTime/4,
              maxDitTime     = 250,
              maxDDWait      = 2*dahTime,
              serialReadWait = 200,
              deounceDelay   = 30,
              maxDd          =4;
    
  public:
    Morser(int buzzerPin, int pbPin, int ledPin = LED_BUILTIN, int toneFreq = NOTE_C4);
    void loop() const;

  private:
    static void a(const Morser &m);
    static void b(const Morser &m);
    static void c(const Morser &m);
    static void d(const Morser &m);
    static void e(const Morser &m);
    static void f(const Morser &m);
    static void g(const Morser &m);
    static void h(const Morser &m);
    static void i(const Morser &m);
    static void j(const Morser &m);
    static void k(const Morser &m);
    static void l(const Morser &m);
    static void m(const Morser &m);
    static void n(const Morser &m);
    static void o(const Morser &m);
    static void p(const Morser &m);
    static void q(const Morser &m);
    static void r(const Morser &m);
    static void s(const Morser &m);
    static void t(const Morser &m);
    static void u(const Morser &m);
    static void v(const Morser &m);
    static void w(const Morser &m);
    static void x(const Morser &m);
    static void y(const Morser &m);
    static void z(const Morser &m);
};

#endif
