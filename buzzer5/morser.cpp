#include "morser.h"

/// will send outgoing and interpret incoming morse code!

void Morser::doTone(const int toneTime,boolean ser = true)  const{
  unsigned long now = millis();
  digitalWrite(ledPin,HIGH);
  tone(buzzerPin, toneFreq, toneTime);
  delay(toneTime);
  unsigned long duration = millis()-now;
  if (ser) {
    Serial.print(duration > maxDitTime ? "dah " : "dit ");
  }
  digitalWrite(ledPin,LOW);
  delay(toneEpsilon);    
}

void Morser::dah() const{
  doTone(dahTime);
}
void Morser::dit() const{
  doTone(ditTime);
}
void Morser::eol() const{
  delay(toneTime);
  Serial.println();
}
 
void Morser::sendWordOut(const String &outWord) const{
  Serial.println(outWord);
  for (int i=0;i<outWord.length();i++){
    doChar(outWord.charAt(i));
  }
  eol();
}

Morser::Morser(int buzPin, int pbbPin, int leddPin, int tonFreq):
  buzzerPin(buzPin),     // buzzer should be connected via resistor to GND
  pbPin(pbbPin),         // pb should be pulled down to GND when pushed
  ledPin(leddPin),        // LED should be connected via resistor to GND
  toneFreq(tonFreq) {
  pinMode(buzzerPin,OUTPUT); 
  pinMode(pbPin,INPUT_PULLUP);
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin,LOW);
}

toneFuncPtr Morser::char2Func(char ci) const{
  static const toneFuncPtr fVec[] = {Morser::a,Morser::b,Morser::c,Morser::d,Morser::e,Morser::f,Morser::g,Morser::h,Morser::i,   
                                     Morser::j,Morser::k,Morser::l,Morser::m,Morser::n,Morser::o,Morser::p,Morser::q,Morser::r,
                                     Morser::s,Morser::t,Morser::u,Morser::v,Morser::w,Morser::x,Morser::y,Morser::z};
                                
  String sVal = String(ci);
  sVal.trim();
  sVal.toLowerCase();
  char cVal = sVal.charAt(0);
  int index = int(cVal)-97;  // convert from ascii with 'a' == 0
  return fVec[index];
} 

void Morser::doChar(char ca) const{
  toneFuncPtr fPtr = char2Func(ca);
  if (fPtr){
    Serial.print(ca);
    Serial.print(" : ");
    (*fPtr)(*this);
    eol();
  }
}

boolean Morser::readFromSerial() const{
  // return false if word is complete and output sent
  static unsigned long lastRead = 0;
  static boolean bRead = false;
  static String word = "";
  if (Serial.available()>0){
    word += String(char(Serial.read()));
    lastRead = millis();
    bRead = true;
  }
  if ((bRead) && (millis() - lastRead > serialReadWait)){
    //Serial.println(String("sending word: ") + word);
    sendWordOut(word);
    word = "";
    bRead = false;
  }
  return bRead;
}

void Morser::loop() const{
  static boolean doNewLine =  false,
                 didSomething = false;
  static int ddCount = 0;
  static long lastOutputTime = millis();
  static const node *currentNodePtr = &start_node;
  if (readFromSerial()){
    doNewLine = true;
  }
  else {
    if (doNewLine && didSomething){  // we finsiehd a full word, so we do new line 
      Serial.println();
      doNewLine = false;  
      didSomething = false;
      lastOutputTime = millis();
    }
  }
  if (!digitalRead(pbPin)){
    delay(30);
    unsigned long now = millis();
    while(! digitalRead(pbPin));
    if (millis()-now > maxDitTime){
      dah();
      currentNodePtr = currentNodePtr ? currentNodePtr->dahPtr : NULL;
    }
    else{
      dit();
      currentNodePtr = currentNodePtr ? currentNodePtr->ditPtr : NULL;
    }
    ddCount++;
    lastOutputTime = millis();
  }
  if (millis()-lastOutputTime > maxDDWait){
    if (currentNodePtr == &start_node){
      NULL;
    }
    else if(currentNodePtr != NULL){
      for (int i=0;i<max(0,maxDd -ddCount);i++){
        Serial.print("    ");
      }
      Serial.print(" : "),
      Serial.print(currentNodePtr->val);
      ddCount=0;
      didSomething = true;
    }
    else{
      Serial.print(" : ERROR !!");
      didSomething = true;
    }
    doNewLine = true;
    currentNodePtr = &start_node;
  }
}
