#ifndef DEFS_H
#define DEFS_H

#include <Arduino.h>

typedef void (*toneFuncPtr)() ;

typedef struct {
  String sName;
  toneFuncPtr fPtrVec[];
} wordOut;

extern void dit();
extern void dah();

extern void a();
extern void b();
extern void c();
extern void d();
extern void e();
extern void f();
extern void g();
extern void h();
extern void i();
extern void j();
extern void k();
extern void l();
extern void m();
extern void n();
extern void o();
extern void p();
extern void q();
extern void r();
extern void s();
extern void t();
extern void u();
extern void v();
extern void w();
extern void x();
extern void y();
extern void z();

#endif
