
/// will send outgoing and interpret incoming morse code!

#include "pitches.h"
#include "defs.h"
#include "node.h"

const int buzzerPin = 8,  // buzzer should be connected via resistor to GND
          pbPin     = 9,  // pb should be pulled down to GND when pushed
          ledPin    = LED_BUILTIN,  // LED should be connected via resistor to GND
          toneTime = 500,
          toneEpsilon = 50,
          dahTime = toneTime-toneEpsilon,
          ditTime = dahTime/4,
          morseNoteIndex = 0,
          maxDitTime = 250,
          maxDDWait  = 2*dahTime,
          serialReadWait = 200;
                
// notes to play:
int notes[] = {NOTE_C4,
               NOTE_D4,
               NOTE_E4,
               NOTE_F4,
               NOTE_G4,
               NOTE_A4, 
               NOTE_B4,
               NOTE_C5},
    nbNotes = 8;

/*
// no longer in use
const wordOut bWord = {"bonjour",{b,o,n,j,o,u,r}},
              cWord = {"ciao",{c,i,a,o}}; 
*/

void doTone(const int i,const int t,boolean ser = true){
  unsigned long now = millis();
  digitalWrite(ledPin,HIGH);
  tone(buzzerPin, notes[i],t);
  delay(t);
  unsigned long duration = millis()-now;
  if (ser) {
    Serial.print(duration > maxDitTime ? "dah " : "dit ");
  }
  digitalWrite(ledPin,LOW);
  delay(toneEpsilon);    
}

void dah(){
  doTone(morseNoteIndex,dahTime);
}
void dit(){
  doTone(morseNoteIndex,ditTime);
}
void eol(){
  delay(toneTime);
  Serial.println();
}
 
void sendWordOut(const String &outWord){
  Serial.println(outWord);
  for (int i=0;i<outWord.length();i++){
    doChar(outWord.charAt(i));
  }
  eol();
}

void setup() {
  Serial.begin(115200);
  pinMode(buzzerPin,OUTPUT); 
  pinMode(pbPin,INPUT_PULLUP);
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin,LOW);
  Serial.println("\nEnter letters to send, or tap morse on the button!");
  Serial.println("Ready!");
}

toneFuncPtr char2Func(char ci){
  static const toneFuncPtr fVec[] = {a,b,c,d,e,f,g,h,i,   // g h i
                                     j,k,l,m,n,o,p,q,r,s,   // p q r
                                     t,u,v,w,x,y,z};
                                
  String sVal = String(ci);
  sVal.trim();
  sVal.toLowerCase();
  char cVal = sVal.charAt(0);
  int index = int(cVal)-97;  // convert from ascii with 'a' == 0
  return fVec[index];
} 

void doChar(char ca){
  toneFuncPtr fPtr = char2Func(ca);
  if (fPtr){
    Serial.print(ca);
    Serial.print(" : ");
    (*fPtr)();
    eol();
  }
}

boolean readFromSerial(){
  // return false if word is complete and output sent
  static unsigned long lastRead = 0;
  static boolean bRead = false;
  static String word = "";
  if (Serial.available()>0){
    word += String(char(Serial.read()));
    lastRead = millis();
    bRead = true;
  }
  if ((bRead) && (millis() - lastRead > serialReadWait)){
    //Serial.println(String("sending word: ") + word);
    sendWordOut(word);
    word = "";
    bRead = false;
  }
  return bRead;
}

void loop(){
  static boolean doNewLine =  false,
                 didSomething = false;
  static long lastOutputTime = millis();
  static const node *currentNodePtr = &start_node;
  if (readFromSerial()){
    doNewLine = true;
  }
  else {
    if (doNewLine && didSomething){  // we finsiehd a full word, so we do new line 
      Serial.println();
      doNewLine = false;  
      didSomething = false;
      lastOutputTime = millis();
    }
  }
  if (!digitalRead(pbPin)){
    delay(30);
    unsigned long now = millis();
    while(! digitalRead(pbPin));
    if (millis()-now > maxDitTime){
      dah();
      currentNodePtr = currentNodePtr->dahPtr;
    }
    else{
      dit();
      currentNodePtr = currentNodePtr->ditPtr;
    }
    lastOutputTime = millis();
  }
    if (millis()-lastOutputTime > maxDDWait){
      if (currentNodePtr == &start_node){
        NULL;
      }
      else if(currentNodePtr != NULL){
        Serial.print(" : "),
        Serial.println(currentNodePtr->val);
        didSomething = true;
      }
      else{
        Serial.println(" : ERROR !!");
        didSomething = true;
      }
      doNewLine = true;
      currentNodePtr = &start_node;
    }
}
