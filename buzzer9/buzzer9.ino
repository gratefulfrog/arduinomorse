
#include "pitches.h"

const int buzzerPin = 8,
          pbPin     = 9,
          ledPin    = LED_BUILTIN,
          toneTime = 500;
          
// notes to play:
int notes[] = {NOTE_C4,
               NOTE_D4,
               NOTE_E4,
               NOTE_F4,
               NOTE_G4,
               NOTE_A4, 
               NOTE_B4,
               NOTE_C5},
    nbNotes = 8;


void setup() {
  pinMode(buzzerPin,OUTPUT); 
  pinMode(pbPin,INPUT_PULLUP);
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin,LOW);
}

void doTone(int i){
  digitalWrite(ledPin,HIGH);
  tone(buzzerPin, notes[i],toneTime);
  delay(toneTime);
  digitalWrite(ledPin,LOW);
  delay(50);  
}

int getInc(int i){
  static int res = 1;
  if (i == nbNotes -1){
    res = -1;
  }
  else if (i == 0){
    res = 1;
  }
  return res;
}

int i = 0;
void loop(){
  if (!digitalRead(pbPin)){
    doTone(i);
    i+= getInc(i);
  }
}

/*

void loop() {
  for (int i = 0; i< nbNotes; i++){
      digitalWrite(ledPin,HIGH);
      tone(buzzerPin, notes[i],1000);
      delay(1000);
      digitalWrite(ledPin,LOW);
      delay(50);
  }
  
  for (int i = nbNotes -1;i>=0; i--){
      digitalWrite(ledPin,HIGH);
      tone(buzzerPin, notes[i],1000);
      delay(1000);
      digitalWrite(ledPin,LOW);
      delay(50);  
  }
}
*/
