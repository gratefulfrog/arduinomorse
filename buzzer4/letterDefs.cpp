#include "morser.h"


void Morser::a(const Morser &mm){
  mm.dit();
  mm.dah();
}
void Morser::b(const Morser &mm){
  mm.dah();
  mm.dit();
  mm.dit();
  mm.dit();
}
void Morser::c(const Morser &mm){
  mm.dah();
  mm.dit();
  mm.dah();
  mm.dit();
}
void Morser::d(const Morser &mm){
  mm.dah();
  mm.dit();
  mm.dit();
}
void Morser::e(const Morser &mm){
  mm.dit();
}
void Morser::f(const Morser &mm){
  mm.dit();
  mm.dit();
  mm.dah();
  mm.dit();
}
void Morser::g(const Morser &mm){
  mm.dah();
  mm.dah();
  mm.dit();
}
void Morser::h(const Morser &mm){
  mm.dit();
  mm.dit();
  mm.dit();
  mm.dit();
}
void Morser::i(const Morser &mm){
  mm.dit();
  mm.dit();
}
void Morser::j(const Morser &mm){
  mm.dit(); 
  mm.dah(); 
  mm.dah(); 
  mm.dah(); 
}
void Morser::k(const Morser &mm){
  mm.dah();
  mm.dit();
  mm.dah();
}
void Morser::l(const Morser &mm){
  mm.dit();
  mm.dah();
  mm.dit();
  mm.dit();
}
void Morser::m(const Morser &mm){
  mm.dah();
  mm.dah();
}
void Morser::n(const Morser &mm){
  mm.dah(); 
  mm.dit(); 
}
void Morser::o(const Morser &mm){
  mm.dah(); 
  mm.dah(); 
  mm.dah();  
}
void Morser::p(const Morser &mm){
  mm.dit();
  mm.dah();
  mm.dah();
  mm.dit();
}
void Morser::q(const Morser &mm){
  mm.dah();
  mm.dah();
  mm.dit();
  mm.dah();
}
void Morser::r(const Morser &mm){
  mm.dit(); 
  mm.dah(); 
  mm.dit(); 
}
void Morser::s(const Morser &mm){
  mm.dit();
  mm.dit();
  mm.dit();
}
void Morser::t(const Morser &mm){
  mm.dah();
}
void Morser::u(const Morser &mm){
  mm.dit(); 
  mm.dit(); 
  mm.dah(); 
}
void Morser::v(const Morser &mm){
  mm.dit();
  mm.dit();
  mm.dit();
  mm.dah();
}
void Morser::w(const Morser &mm){
  mm.dit();
  mm.dah();
  mm.dah();
}
void Morser::x(const Morser &mm){
  mm.dah();
  mm.dit();
  mm.dit();
  mm.dah();
}
void Morser::y(const Morser &mm){
  mm.dah();
  mm.dit();
  mm.dah();
  mm.dah();
}
void Morser::z(const Morser &mm){
  mm.dah();
  mm.dah();
  mm.dit();
  mm.dit();
}
