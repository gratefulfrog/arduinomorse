#ifndef MORSER_H
#define MORSER_H

#include <Arduino.h>
#include "pitches.h"
#include "node.h"

class Morser;
typedef void (*toneFuncPtr)(const Morser&);

class Morser {
   private:
    void doTone(const int toneTime,boolean ser = true)  const;
    void dah() const;
    void dit() const; 
    void eol() const;
    void sendWordOut(const String &outWord) const;
    toneFuncPtr char2Func(char ci) const;
    void doChar(char ca) const;
    boolean readFromSerial() const;
    void interpretPbPress() const;

    const int buzzerPin,     // buzzer should be connected via resistor to GND
              pbPin,         // pb should be pulled down to GND when pushed
              ledPin,        // LED should be connected via resistor to GND
              toneFreq,      // tone frequency to buzz
              toneTime       = 500,
              toneEpsilon    = 50,
              dahTime        = toneTime-toneEpsilon,
              ditTime        = dahTime/4,
              maxDitTime     = 250,
              maxDDWait      = 2*dahTime,
              serialReadWait = 200,
              deounceDelay   = 30,
              maxDd          =4;
    
  public:
    Morser(int buzzerPin, int pbPin, int ledPin = LED_BUILTIN, int toneFreq = NOTE_C4);
    void loop() const;

  private:
    static void a(const Morser&);
    static void b(const Morser&);
    static void c(const Morser&);
    static void d(const Morser&);
    static void e(const Morser&);
    static void f(const Morser&);
    static void g(const Morser&);
    static void h(const Morser&);
    static void i(const Morser&);
    static void j(const Morser&);
    static void k(const Morser&);
    static void l(const Morser&);
    static void m(const Morser&);
    static void n(const Morser&);
    static void o(const Morser&);
    static void p(const Morser&);
    static void q(const Morser&);
    static void r(const Morser&);
    static void s(const Morser&);
    static void t(const Morser&);
    static void u(const Morser&);
    static void v(const Morser&);
    static void w(const Morser&);
    static void x(const Morser&);
    static void y(const Morser&);
    static void z(const Morser&);
};

#endif
