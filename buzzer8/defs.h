#ifndef DEFS_H
#define DEFS_H

typedef struct {
  String sName;
  void (*fPtrVec[])();
} wordOut;

extern void dit();
extern void dah();

void a(){
  dit();
  dah();
}
void b(){
  dah();
  dit();
  dit();
}
void c(){
  dah();
  dit();
  dah();
  dit();
}
void i(){
  dit();
  dit();
}
void j(){
  dit(); 
  dah(); 
  dah(); 
  dah(); 
}
void o(){
  dah(); 
  dah(); 
  dah();  
}
void n(){
  dah(); 
  dit(); 
}
void r(){
  dit(); 
  dah(); 
  dit(); 
}
void u(){
  dit(); 
  dit(); 
  dah(); 
}

#endif
