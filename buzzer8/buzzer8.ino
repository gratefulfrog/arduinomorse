
#include "pitches.h"
#include "defs.h"

const int buzzerPin = 8,
          pbPin     = 9,
          ledPin    = LED_BUILTIN,
          toneTime = 500,
          toneEpsilon = 50,
          dahTime = toneTime-toneEpsilon,
          ditTime = dahTime/4,
          morseNoteIndex = 0,
          maxDitTime = 250;
                
// notes to play:
int notes[] = {NOTE_C4,
               NOTE_D4,
               NOTE_E4,
               NOTE_F4,
               NOTE_G4,
               NOTE_A4, 
               NOTE_B4,
               NOTE_C5},
    nbNotes = 8;

void doTone(const int i,const int t,boolean ser = true){
  unsigned long now = millis();
  digitalWrite(ledPin,HIGH);
  tone(buzzerPin, notes[i],t);
  delay(t);
  unsigned long duration = millis()-now;
  if (ser) {
    Serial.print(duration > maxDitTime ? "dah " : "dit ");
  }
  digitalWrite(ledPin,LOW);
  delay(toneTime-t);  
}
void dah(){
  doTone(morseNoteIndex,dahTime);
}
void dit(){
  doTone(morseNoteIndex,ditTime);
}
void eol(){
  delay(toneTime);
  Serial.println();
}

const wordOut bWord = {"bonjour",{b,o,n,j,o,u,r}},
              cWord = {"ciao",{c,i,a,o}}; 
 
void sendWordOut(const wordOut &outWord){
  for (int i=0;i<outWord.sName.length();i++){
    (*outWord.fPtrVec[i])();  
    eol();
  }
  eol();
}

void setup() {
  Serial.begin(115200);
  pinMode(buzzerPin,OUTPUT); 
  pinMode(pbPin,INPUT_PULLUP);
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin,LOW);
  sendWordOut(bWord);
  sendWordOut(cWord);
}

int getInc(const int i){
  static int res = 1;
  if (i == nbNotes -1){
    res = -1;
  }
  else if (i == 0){
    res = 1;
  }
  return res;
}

void loop(){
  static int ii = 0;
  if (!digitalRead(pbPin)){
    doTone(ii,toneTime,false);
    ii+= getInc(ii);
  }
}
